# Blog Dashboard

This Projected developed with [vuejs 3](https://vuejs.org/) and Typescript.

### Project Structure

Global scoped components are in `src/libs/design` directory and page specific
components are under `page/components` directory.

### Orval

**[Orval](https://orval.dev/overview)** is used to generate form schemas, types
and server actions. For updating orval you should change api.yaml.
