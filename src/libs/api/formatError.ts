export const formatError = (err: any) => {
  if (typeof err === 'object')
    return Object.entries(err)
      .map(item => item.join(': '))
      .join('\n');
  else if (typeof err === 'string') return err;
  return 'something went wrong!';
};
