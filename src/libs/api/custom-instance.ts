import axios from 'axios';

export const customInstance = axios.create({
  baseURL: import.meta.env.VITE_APP_API_ENDPOINT,
});
