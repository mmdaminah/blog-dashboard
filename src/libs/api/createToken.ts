import { useAuthStore } from '../store/authStore';

const { token } = useAuthStore();

export const createToken = () => `Token ${token}`;
