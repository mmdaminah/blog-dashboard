import { defineStore } from 'pinia';
import { ref } from 'vue';
import type { User } from '@/libs/dataLayer/model';

export const useAuthStore = defineStore('auth', () => {
  const token = ref('');
  const user = ref<User | null>();

  function setToken(authToken: string) {
    token.value = authToken;
  }
  function setUser(verifiedUser: User) {
    user.value = verifiedUser;
  }
  function logout() {
    token.value = '';
    user.value = null;
  }

  return { token, user, setToken, setUser, logout };
});
