import { defineStore } from 'pinia';

type ToastStatus = 'success' | 'error';

interface Toast {
  text: string;
  status: ToastStatus;
}
type ToastPayload = { text: string };

const defaultTimeout = 3000;

export const useToasterStore = defineStore('toaster-store', {
  state: (): { show: boolean; toast: Toast } => ({
    show: false,
    toast: {
      text: 'text',
      status: 'error',
    },
  }),
  actions: {
    success(payload: ToastPayload) {
      this.toast.text = payload.text;
      this.toast.status = 'success';
      this.show = true;
      setTimeout(() => {
        this.show = false;
      }, defaultTimeout);
    },

    error(payload: ToastPayload) {
      this.toast.text = payload.text;
      this.toast.status = 'error';
      this.show = true;
      setTimeout(() => {
        this.show = false;
      }, defaultTimeout);
    },
    close() {
      this.show = false;
    },
  },
});
