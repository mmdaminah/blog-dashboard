/**
 * Generated by orval v6.30.2 🍺
 * Do not edit manually.
 * RealWorld Conduit API
 * Conduit API documentation
 * OpenAPI spec version: 1.0.0
 */
import * as axios from 'axios';
import type { AxiosRequestConfig, AxiosResponse } from 'axios';
import type {
  EmptyOkResponseResponse,
  MultipleCommentsResponseResponse,
  NewCommentRequestBody,
  SingleCommentResponseResponse,
} from '../../model';

export const getComments = () => {
  /**
   * Get the comments for an article. Auth is optional
   * @summary Get comments for an article
   */
  const getArticleComments = <
    TData = AxiosResponse<MultipleCommentsResponseResponse>,
  >(
    slug: string,
    options?: AxiosRequestConfig,
  ): Promise<TData> => {
    return axios.default.get(
      `https://api.realworld.io/api/articles/${slug}/comments`,
      options,
    );
  };
  /**
   * Create a comment for an article. Auth is required
   * @summary Create a comment for an article
   */
  const createArticleComment = <
    TData = AxiosResponse<SingleCommentResponseResponse>,
  >(
    slug: string,
    newCommentRequestBody: NewCommentRequestBody,
    options?: AxiosRequestConfig,
  ): Promise<TData> => {
    return axios.default.post(
      `https://api.realworld.io/api/articles/${slug}/comments`,
      newCommentRequestBody,
      options,
    );
  };
  /**
   * Delete a comment for an article. Auth is required
   * @summary Delete a comment for an article
   */
  const deleteArticleComment = <TData = AxiosResponse<EmptyOkResponseResponse>>(
    slug: string,
    id: number,
    options?: AxiosRequestConfig,
  ): Promise<TData> => {
    return axios.default.delete(
      `https://api.realworld.io/api/articles/${slug}/comments/${id}`,
      options,
    );
  };
  return { getArticleComments, createArticleComment, deleteArticleComment };
};
export type GetArticleCommentsResult =
  AxiosResponse<MultipleCommentsResponseResponse>;
export type CreateArticleCommentResult =
  AxiosResponse<SingleCommentResponseResponse>;
export type DeleteArticleCommentResult = AxiosResponse<EmptyOkResponseResponse>;
