/**
 * Generated by orval v6.30.2 🍺
 * Do not edit manually.
 * RealWorld Conduit API
 * Conduit API documentation
 * OpenAPI spec version: 1.0.0
 */
import { z as zod } from 'zod';

/**
 * Get most recent articles from users you follow. Use query parameters to limit. Auth is required
 * @summary Get recent articles from users you follow
 */
export const getArticlesFeedQueryOffsetMin = 0;

export const getArticlesFeedQueryParams = zod.object({
  offset: zod.number().min(getArticlesFeedQueryOffsetMin).optional(),
  limit: zod.number().min(1).optional(),
});

export const getArticlesFeedResponse = zod.object({
  articles: zod.array(
    zod.object({
      slug: zod.string(),
      title: zod.string(),
      description: zod.string(),
      body: zod.string(),
      tagList: zod.array(zod.string()),
      createdAt: zod.string().datetime(),
      updatedAt: zod.string().datetime(),
      favorited: zod.boolean(),
      favoritesCount: zod.number(),
      author: zod.object({
        username: zod.string(),
        bio: zod.string(),
        image: zod.string(),
        following: zod.boolean(),
      }),
    }),
  ),
  articlesCount: zod.number(),
});

/**
 * Get most recent articles globally. Use query parameters to filter results. Auth is optional
 * @summary Get recent articles globally
 */
export const getArticlesQueryOffsetMin = 0;

export const getArticlesQueryParams = zod.object({
  tag: zod.string().optional(),
  author: zod.string().optional(),
  favorited: zod.string().optional(),
  offset: zod.number().min(getArticlesQueryOffsetMin).optional(),
  limit: zod.number().min(1).optional(),
});

export const getArticlesResponse = zod.object({
  articles: zod.array(
    zod.object({
      slug: zod.string(),
      title: zod.string(),
      description: zod.string(),
      body: zod.string(),
      tagList: zod.array(zod.string()),
      createdAt: zod.string().datetime(),
      updatedAt: zod.string().datetime(),
      favorited: zod.boolean(),
      favoritesCount: zod.number(),
      author: zod.object({
        username: zod.string(),
        bio: zod.string(),
        image: zod.string(),
        following: zod.boolean(),
      }),
    }),
  ),
  articlesCount: zod.number(),
});

/**
 * Create an article. Auth is required
 * @summary Create an article
 */
export const createArticleBody = zod.object({
  article: zod.object({
    title: zod.string(),
    description: zod.string(),
    body: zod.string(),
    tagList: zod.array(zod.string()).optional(),
  }),
});

export type CreateArticleBody = zod.infer<typeof createArticleBody>;

/**
 * Get an article. Auth not required
 * @summary Get an article
 */
export const getArticleParams = zod.object({
  slug: zod.string(),
});

export const getArticleResponse = zod.object({
  article: zod.object({
    slug: zod.string(),
    title: zod.string(),
    description: zod.string(),
    body: zod.string(),
    tagList: zod.array(zod.string()),
    createdAt: zod.string().datetime(),
    updatedAt: zod.string().datetime(),
    favorited: zod.boolean(),
    favoritesCount: zod.number(),
    author: zod.object({
      username: zod.string(),
      bio: zod.string(),
      image: zod.string(),
      following: zod.boolean(),
    }),
  }),
});

/**
 * Update an article. Auth is required
 * @summary Update an article
 */
export const updateArticleParams = zod.object({
  slug: zod.string(),
});

export const updateArticleBody = zod.object({
  article: zod.object({
    title: zod.string().optional(),
    description: zod.string().optional(),
    body: zod.string().optional(),
  }),
});

export type UpdateArticleBody = zod.infer<typeof updateArticleBody>;

export const updateArticleResponse = zod.object({
  article: zod.object({
    slug: zod.string(),
    title: zod.string(),
    description: zod.string(),
    body: zod.string(),
    tagList: zod.array(zod.string()),
    createdAt: zod.string().datetime(),
    updatedAt: zod.string().datetime(),
    favorited: zod.boolean(),
    favoritesCount: zod.number(),
    author: zod.object({
      username: zod.string(),
      bio: zod.string(),
      image: zod.string(),
      following: zod.boolean(),
    }),
  }),
});

/**
 * Delete an article. Auth is required
 * @summary Delete an article
 */
export const deleteArticleParams = zod.object({
  slug: zod.string(),
});
