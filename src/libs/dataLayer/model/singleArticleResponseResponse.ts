/**
 * Generated by orval v6.30.2 🍺
 * Do not edit manually.
 * RealWorld Conduit API
 * Conduit API documentation
 * OpenAPI spec version: 1.0.0
 */
import type { Article } from './article';

export type SingleArticleResponseResponse = {
  article: Article;
};
