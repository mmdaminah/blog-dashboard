import { useAuthStore } from '@/libs/store/authStore';
import { createRouter, createWebHistory } from 'vue-router';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    { path: '/', redirect: { path: '/login' } },
    {
      path: '/register',
      name: 'register',
      component: () => import('@/pages/Register/RegisterPage.vue'),
      meta: {
        layout: 'AuthLayout',
      },
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../pages/Login/LoginPage.vue'),
      meta: {
        layout: 'AuthLayout',
      },
    },
    {
      path: '/articles',
      name: 'articles',
      component: () => import('@/pages/Articles/List/ListPage.vue'),
      meta: {
        layout: 'MainLayout',
      },
    },
    {
      path: '/articles/page/:page',
      name: 'articles-page',
      component: () => import('@/pages/Articles/List/ListPage.vue'),
      meta: {
        layout: 'MainLayout',
      },
    },
    {
      path: '/articles/create',
      name: 'create-article',
      component: () => import('@/pages/Articles/Create/CreatePage.vue'),
      meta: {
        layout: 'MainLayout',
      },
    },
    {
      path: '/articles/edit/:slug',
      name: 'edit-article',
      component: () => import('@/pages/Articles/Edit/EditPage.vue'),
      meta: {
        layout: 'MainLayout',
      },
    },
  ],
});

router.beforeEach((to, from, next) => {
  const { user } = useAuthStore();

  if (user || to.name === 'login' || to.name === 'register') {
    next();
  } else {
    next({ name: 'login' });
  }
});

export default router;
