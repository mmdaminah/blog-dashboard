import { defineConfig } from 'orval';

export default defineConfig({
  apiSchema: {
    output: {
      mode: 'tags-split',
      tsconfig: './tsconfig.json',
      // override: {
      //   mutator: {
      //     path: './src/libs/api/custom-instance.ts',
      //     name: 'customInstance',
      //   },
      // },
      baseUrl: 'https://api.realworld.io/api',
      target: 'src/libs/dataLayer/operations',
      schemas: 'src/libs/dataLayer/model',
      client: 'axios',
      mock: false,
      urlEncodeParameters: true,
    },
    input: {
      target: './api.yaml',
    },
  },
});
